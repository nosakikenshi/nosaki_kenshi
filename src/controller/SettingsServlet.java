package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import service.UserService;

@WebServlet(urlPatterns = { "/settings" })
public class SettingsServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

    	int user_id = Integer.parseInt(request.getParameter("user_id"));
        User editUser = new UserService().getUser(user_id);
        request.setAttribute("editUser", editUser);

        request.getRequestDispatcher("settings.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        List<String> messages = new ArrayList<String>();
        HttpSession session = request.getSession();
        User editUser = getEditUser(request);

        if (isValid(request, messages) == true) {

            try {
                new UserService().update(editUser);
            } catch (NoRowsUpdatedRuntimeException e) {
                messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
                session.setAttribute("errorMessages", messages);
                request.setAttribute("editUser", editUser);
                request.getRequestDispatcher("settings.jsp").forward(request, response);
                return;
            }

            session.setAttribute("loginUser", editUser);

            response.sendRedirect("settings");
        } else {
            session.setAttribute("errorMessages", messages);
            request.setAttribute("editUser", editUser);
            request.getRequestDispatcher("settings.jsp").forward(request, response);
        }
    }

    private User getEditUser(HttpServletRequest request)
            throws IOException, ServletException {

        User editUser = new User();
        editUser.setId(Integer.parseInt(request.getParameter("id")));
        editUser.setLoginId(request.getParameter("loginId"));
        editUser.setPassword(request.getParameter("password"));
        editUser.setName(request.getParameter("name"));
        editUser.setBranch(request.getParameter("branch"));
        editUser.setPosition(request.getParameter("position"));
        return editUser;
    }


    private boolean isValid(HttpServletRequest request, List<String> messages) {

        String loginId = request.getParameter("loginId");
        String password = request.getParameter("password");
        String name = request.getParameter("name");
        String confPass = request.getParameter("confPass");

        System.out.print(loginId);

        if (StringUtils.isEmpty(loginId) == true) {
            messages.add("ログインIDを入力してください");
        } else if (!loginId.matches("[a-zA-Z0-9]{6,20}")) {
        	messages.add("ログインIDは半角英数字、6文字以上20文字以内");
        }
        if (StringUtils.isEmpty(password) == true) {
            messages.add("パスワードを入力してください");
        } else if (!password.matches("[ -~]{6,20}")) {
        	messages.add("パスワードは半角英数字・記号、6文字以上20文字以内");
        } else if (!password.equals(confPass)) {
        	messages.add("パスワードの不一致");
        }
        if (name.length() >= 10) {
        	messages.add("名称は10文字以下とします");
        }
        // TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}